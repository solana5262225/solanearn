import { FormControl, FormLabel, Stack, Input, Button } from "@chakra-ui/react";
import { useState } from "react";
import { CreateVote } from "../helpers/ProposalHelper";
import { useAnchorWallet, useConnection } from "@solana/wallet-adapter-react";
import styles from "../../styles/Home.module.css";

export default function ProposalForm() {
  const [title, setTitle] = useState();
  const [description, setDescription] = useState();
  const [options, setOptions] = useState();
  const connection = useConnection();
  const wallet = useAnchorWallet();

  const handleTitle = (event) => {
    setTitle(event.target.value);
  };

  const handleDescription = (event) => {
    setDescription(event.target.value);
  };

  const handleOptions = (event) => {
    setOptions(event.target.value);
  };

  const submitProposal = () => {
    CreateVote(title, description, options, connection.connection, wallet);
  };
  return (
    <div>
      <h1 className={styles.appTitle}>DAO Vote - proposal</h1>
      <FormControl>
        <Stack spacing={3} padding={3}>
          <FormLabel color="white">Proposal title</FormLabel>
          <Input
            placeholder="Proposal title"
            size="md"
            onChange={handleTitle}
          />

          <FormLabel color="white">Proposal description</FormLabel>
          <Input
            placeholder="Proposal description"
            size="md"
            onChange={handleDescription}
          />

          <FormLabel color="white">Proposal options</FormLabel>
          <Input
            placeholder="Porposal options"
            size="md"
            onChange={handleOptions}
          />
          <Button
            mt={4}
            colorScheme="teal"
            type="submit"
            onClick={submitProposal}
          >
            Create vote
          </Button>
        </Stack>
      </FormControl>
    </div>
  );
}
