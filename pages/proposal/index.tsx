import { Box, Stack } from "@chakra-ui/react";
import { useWallet } from "@solana/wallet-adapter-react";
import { WalletMultiButton } from "@solana/wallet-adapter-react-ui";
import Head from "next/head";
import { AppBar } from "../../components/AppBar";
import Header from "../../components/header/Header";
import styles from "../../styles/Home.module.css";
import ProposalDrawer from "./ProposalDrawer";

export default function Proposal() {
  const wallet = useWallet();

  return (
    <div className={styles.App}>
      <Head>
        <title>Proposal</title>
      </Head>
      <Box h="calc(100vh)" w="full">
        <Stack w="full" h="calc(100vh)" justify="center">
          <AppBar />
          <Header />
          <div className={styles.AppBody}>
            {wallet.connected ? (
              <div className={styles.proposalDrawer}>
                <ProposalDrawer />
              </div>
            ) : (
              <WalletMultiButton />
            )}
          </div>
        </Stack>
      </Box>
    </div>
  );
}
