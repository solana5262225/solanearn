import { getProgramById } from "./ProgramHelper";
import * as anchor from "@coral-xyz/anchor";
import BN from 'bn.js';
import * as web3 from "@solana/web3.js";
import { Keypair } from "@solana/web3.js";

export const CreateVote = async (title, description, options, connection, wallet) => {
    console.log('from here');

    // get the program
    const VOTE_DAO_PROGRAM_ID = "AtCaMdfkramjt4rqciAvZn3HCaxeFubhMJReR2CoadC5";
    const program = getProgramById(connection, wallet, VOTE_DAO_PROGRAM_ID);
    console.log('the id ->', program.programId.toBase58());

    // get the PDA address
    const [proposal, _proposalBump] =
        await anchor.web3.PublicKey.findProgramAddress(
            [wallet.publicKey.toBytes()],
            program.programId
        );

    console.log('proposal', proposal);
    console.log('_proposalBump', _proposalBump);

    // generate deadline
    const currentYear = new Date().getFullYear();
    const endOfYearDate = new Date(currentYear, 11, 31); // Note that month is 0-indexed, so 11 represents December
    endOfYearDate.setUTCHours(23, 59, 59, 999); // Set to the last millisecond of the day
    const finalTimeStamp = Math.floor(endOfYearDate.getTime() / 1000); // Convert to seconds since Unix epoch
    const deadline = new BN.BN(finalTimeStamp);

    // create & send tx
    const SYSTEM_PROGRAM_ID = '11111111111111111111111111111111';
    const systemProgramId = new web3.PublicKey(SYSTEM_PROGRAM_ID);
    const proposalKeypair = Keypair.generate();

    // prepare options tab
    const optionsTab = options.split(',', 10);

    const tx = await program.methods
        .createProposal(title, description, optionsTab, deadline)
        .accounts({
            proposal: proposalKeypair.publicKey,
            signer: wallet.publicKey,
            systemProgram: systemProgramId,
        })
        .signers([proposalKeypair])
        .rpc();

    console.log('tx', tx);
}
