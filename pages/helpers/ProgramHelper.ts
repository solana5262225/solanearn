import * as anchor from "@coral-xyz/anchor";
import proposalIdl from '../../dao_vote_idl.json';

export const getProgramById = (connection, wallet, programId) => {
    
    let provider: anchor.Provider;
    try {
      provider = new anchor.AnchorProvider(connection, wallet, {});
    } catch {
      provider = anchor.getProvider();
    }
    anchor.setProvider(provider);
    const program = new anchor.Program(proposalIdl as anchor.Idl, provider);
    return program;
}